package com.peterd.zwitch;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

public class utils {
	// Parameter
	
	// Constructor
	
	public utils() {
	}

	// Methoden
	
	// Copy a File
/**	public void copyFile(String src, String dst) {
		FileChannel inChannel = null; 
		FileChannel outChannel = null;
		try {
			inChannel = new FileInputStream(src).getChannel();
	    	outChannel = new FileOutputStream(dst).getChannel();
	    	inChannel.transferTo(0, inChannel.size(), outChannel);
	    	if (inChannel != null) inChannel.close();
	    	if (outChannel != null) outChannel.close();
	    } 
	    catch (IOException error){
	    	System.out.println(error);
	    	Toast.makeText(null, "ERROR: copy build.prop", Toast.LENGTH_LONG).show();
	    } 
		finally {
			//
		}
	} */
	// Copy a File - ENDE
	
	// Remount 
	public boolean remount(String as) {
		try {
			Process p_remount;
			p_remount = Runtime.getRuntime().exec("su"); 
			DataOutputStream os = new DataOutputStream(p_remount.getOutputStream());
			if (as.contentEquals("rw"))
				os.writeBytes("mount -ro remount,rw /system\n");
			if (as.contentEquals("ro"))
				os.writeBytes("mount -ro remount,ro /system\n");
			os.writeBytes("exit\n");
			os.flush();
			p_remount.waitFor();
			return true;
			//Toast.makeText(null, "remount as " + as, Toast.LENGTH_SHORT).show();
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.v("pd_", e.toString());
			System.out.println(e);
			return false;
			//Toast.makeText(null, "Error: remount...", Toast.LENGTH_LONG).show();
		}
		finally {
			//
		}
	}
	// Remount - ENDE
	
	// Rewrite
	public boolean rewrite(String quelle, String ziel, Boolean bool_dpi, String dpi, Boolean bool_clientid, String clientid) {
		String ZeilenInhalt = null;
		try {
			BufferedReader in = new BufferedReader(new FileReader(quelle));
			BufferedWriter out = new BufferedWriter(new FileWriter(ziel));
			while ((ZeilenInhalt = in.readLine()) != null) {
				if (ZeilenInhalt.contains("ro.sf.lcd_density="))
					if (bool_dpi==true)
						ZeilenInhalt = "ro.sf.lcd_density=" + dpi;		
				if (ZeilenInhalt.contains("ro.com.google.clientidbase="))
					if (bool_clientid==true)
						ZeilenInhalt = "ro.com.google.clientidbase=" + clientid;
				out.write(ZeilenInhalt + "\n");
			}
			in.close();
			out.close();
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.v("pd_", e.toString());
			System.out.println(e);
			return false;
		}
		finally {
			//
		}
	}
	// Rewrite - ENDE
	
	// copyfile
	public boolean copyfile(String quelle, String ziel) {
		Process p_copy;
		try {
		p_copy = Runtime.getRuntime().exec("su"); 
		   DataOutputStream os = new DataOutputStream(p_copy.getOutputStream());
		   os.writeBytes("cp " + quelle + " " + ziel + "\n");
		   os.writeBytes("exit\n");
		   os.flush();
		   p_copy.waitFor();
		   p_copy.destroy();
		   return true;
		} 
		catch (Exception e) {
			e.printStackTrace();
			Log.v("pd_", e.toString());
			System.out.println(e);
			return false;
		}
		finally {
			//
		}
	}
	// copyfile - ENDE
	
	// deletefile
	public boolean deletefile(String datei) {
		try {
			File file = new File(datei);
			file.delete();
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.v("pd_", e.toString());
			System.out.println(e);
			return false;
		}
		finally {
			//
		}
		
	}
	//deletefile - ENDE
	
	// reboot
	public void reboot(MainActivity mainactivity) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mainactivity);
		alertDialogBuilder.setTitle("Reboot?");
		alertDialogBuilder
		.setMessage("Neustart durchführen?")
		.setCancelable(false)
		.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				//MainActivity.this.finish();
				try {
					Runtime.getRuntime().exec(new String[]{"/system/bin/su","-c","reboot now"});
				} catch (IOException e) {
					e.printStackTrace();
					Log.v("pd_", e.toString());
					System.out.println(e);
				}
			}
		  })
		.setNegativeButton("No",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			}
		});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();
		// show it
		alertDialog.show();
	}
	// reboot - ENDE
	
	// Schreibstatus
	public void schreibstatus(String[] statuswert) {
		String ZeilenInhalt = null;
		Process p_schreibstatus;
		try {
			p_schreibstatus = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(p_schreibstatus.getOutputStream());
			os.writeBytes("cat /proc/mounts | grep /system > " + Environment.getExternalStorageDirectory().getPath()+"/pd_mountstatus" + "\n");
			os.writeBytes("exit\n");
			os.flush();
			BufferedReader in = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath()+"/pd_mountstatus"));
			while ((ZeilenInhalt = in.readLine()) != null) {
				//System.out.println("Gelesene Zeile: " + zeile);
				if (ZeilenInhalt.contains("ro"))
					//textview3.setText("ro");
					statuswert[0] = "ro";
				if (ZeilenInhalt.contains("rw"))
					//textview3.setText("rw");
					statuswert[0] = "rw";
			}
			in.close(); 
			p_schreibstatus.waitFor();
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.v("pd_", e.toString());
			System.out.println(e);
			statuswert[0] = "ERROR";
		}
		finally {
			//
		}
	}
	// Schreibstatus - ENDE
	
	// readfile
	public void readfile(String datei, String readwerte[]) {
		String ZeilenInhalt = null;
		try {
			BufferedReader in = new BufferedReader(new FileReader(datei));
			while ((ZeilenInhalt = in.readLine()) != null) {
				//System.out.println("Gelesene Zeile: " + zeile);
				if (ZeilenInhalt.contains("ro.sf.lcd_density="))
					//textview1.setText(ZeilenInhalt.substring(ZeilenInhalt.lastIndexOf('=') + 1));
					readwerte[0]=ZeilenInhalt.substring(ZeilenInhalt.lastIndexOf('=') + 1);
				if (ZeilenInhalt.contains("ro.com.google.clientidbase="))
					//textview2.setText(ZeilenInhalt.substring(ZeilenInhalt.lastIndexOf('=') + 1));
					readwerte[1]=ZeilenInhalt.substring(ZeilenInhalt.lastIndexOf('=') + 1);
			}
			in.close();		
		} 
		catch (IOException e) {
			e.printStackTrace();
			Log.v("pd_", e.toString());
			System.out.println(e);
			readwerte[0]="ERROR";
			readwerte[1]="ERROR";
		}
		finally {
			//
		}
		}
	// readfile - ENDE	
	
	// getNTP
	public void getNTP(String datei, String[] statusntp) {
		String ZeilenInhalt = null;
		try {
			BufferedReader in = new BufferedReader(new FileReader(datei));
			while ((ZeilenInhalt = in.readLine()) != null) {
				if (ZeilenInhalt.contains("NTP_SERVER="))
					statusntp[0]=ZeilenInhalt.substring(ZeilenInhalt.lastIndexOf('=') + 1);
			}
			in.close();		
		} 
		catch (IOException e) {
			e.printStackTrace();
			Log.v("pd_", e.toString());
			System.out.println(e);
			statusntp[0]="ERROR";
		}
		finally {
			//
		}
	}
	// getNTP - ENDE
	
	// setNTP
	public boolean setNTP(String quelle, String ziel, String wert) {
		String ZeilenInhalt = null;
		try {
			BufferedReader in = new BufferedReader(new FileReader(quelle));
			BufferedWriter out = new BufferedWriter(new FileWriter(ziel));
			while ((ZeilenInhalt = in.readLine()) != null) {
				if (ZeilenInhalt.contains("NTP_SERVER="))
						ZeilenInhalt = "NTP_SERVER==" + wert;		
				out.write(ZeilenInhalt + "\n");
			}
			in.close();
			out.close();
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.v("pd_", e.toString());
			System.out.println(e);
			return false;
		}
		finally {
			//
		}
		if (copyfile(ziel, quelle)) {
			return true;
		}
		else {
			return false;
		}
	}
	// setNTP - ENDE
	
	// setTone
	public boolean setTone(String asType, String file, MainActivity mainactivity) {		
		try {
			mainactivity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://"+file)));
			File k = new File(file);
			ContentValues values = new ContentValues();
			values.put(MediaStore.MediaColumns.DATA, k.getAbsolutePath());
			if (asType=="ringtone") {
				values.put(MediaStore.MediaColumns.TITLE, "Zwitch_Rintone");
				values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
				values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false);
				values.put(MediaStore.Audio.Media.IS_ALARM, false);
			}
			if (asType=="notification") {
				values.put(MediaStore.MediaColumns.TITLE, "Zwitch_Notification");
				values.put(MediaStore.Audio.Media.IS_RINGTONE, false);
				values.put(MediaStore.Audio.Media.IS_NOTIFICATION, true);
				values.put(MediaStore.Audio.Media.IS_ALARM, false);
			}
			if (asType=="alarm") {
				values.put(MediaStore.MediaColumns.TITLE, "Zwitch_Alarm");
				values.put(MediaStore.Audio.Media.IS_RINGTONE, false);
				values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false);
				values.put(MediaStore.Audio.Media.IS_ALARM, true);
			}
			values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
			values.put(MediaStore.Audio.Media.ARTIST, "cssounds ");
			values.put(MediaStore.Audio.Media.IS_MUSIC, false);
			//Insert it into the database
			Uri newUri= mainactivity.getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(k.getAbsolutePath()), values);
			if (asType=="ringtone") {
				RingtoneManager.setActualDefaultRingtoneUri(mainactivity, RingtoneManager.TYPE_RINGTONE, newUri);
			}
			if (asType=="notification") {
				RingtoneManager.setActualDefaultRingtoneUri(mainactivity, RingtoneManager.TYPE_NOTIFICATION, newUri);
			}
			if (asType=="alarm") {
				RingtoneManager.setActualDefaultRingtoneUri(mainactivity, RingtoneManager.TYPE_ALARM, newUri);
			}
			return true;
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.v("pd_", e.toString());
			System.out.println(e);
			return false;
		}
		finally {
			//
		}
	}	
	// setTone - ENDE
	
	// getSMSTone
	public void getSMSTone(String datei, String[] smstone) {
			String ZeilenInhalt = null;
			try {
				BufferedReader in = new BufferedReader(new FileReader(datei));
				while ((ZeilenInhalt = in.readLine()) != null) {
					if (ZeilenInhalt.contains("<string name=\"pref_key_ringtone\">"))
						smstone[0]=ZeilenInhalt.substring(ZeilenInhalt.lastIndexOf('>') + 1);
				}
				in.close();		
			} 
			catch (IOException e) {
				e.printStackTrace();
				Log.v("pd_", e.toString());
				System.out.println(e);
				smstone[0]="ERROR";
			}
			finally {
				//
			}
	}
	// getSMSTone - ENDE
	
	// getSMSToneDirty
	public void getSMSToneDirty(String[] smstone) {
		String ZeilenInhalt = null;
		Process p_getsmstone;
		try {
			p_getsmstone = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(p_getsmstone.getOutputStream());
			os.writeBytes("cat /data/data/com.android.mms/shared_prefs/com.android.mms_preferences.xml | grep pref_key_ringtone > " + Environment.getExternalStorageDirectory().getPath()+"/pd_getsmstone" + "\n");
			os.writeBytes("exit\n");
			os.flush();
			BufferedReader in = new BufferedReader(new FileReader(Environment.getExternalStorageDirectory().getPath()+"/pd_getsmstone"));
			while ((ZeilenInhalt = in.readLine()) != null) {
				//System.out.println("pd_ " + ZeilenInhalt);
				if (ZeilenInhalt.contains("pref_key_ringtone")) {
					String[] seperated = ZeilenInhalt.split(">");
					String[] seperated2 = seperated[1].split("<");
					smstone[0] = seperated2[0];
					// System.out.println("pd_ " + smstone[0]);
				}
			}
			in.close(); 
			p_getsmstone.waitFor();
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.v("pd_", e.toString());
			System.out.println(e);
			smstone[0] = "ERROR";
		}
		finally {
			//
		}
	}
	// getSMSToneDirty - ENDE

}
