package com.peterd.zwitch;

import android.app.Activity;
import android.content.Context;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	Context context = this;
	
	private utils utils = new utils();
	private Button btn_filepicker_ringtone; // btn_akt, btn_uber, btn_beenden
	private TextView textview1, textview2, textview_ringtone, textview_notification, textview_alarm, textView_ntp, textview_sms; //textview3
	private EditText edittext_dpi, edittext_clientid, edittext_ringtone, edittext_notification, edittext_alarm, edittext_ntp;
	private CheckBox checkbox_dpi, checkbox_clientid, checkbox_ringtone, checkbox_notification, checkbox_alarm, checkbox_ntp;
	private ProgressBar progressbar1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// btn_akt = (Button) findViewById(R.id.button_aktualisieren);
		// btn_akt.setOnClickListener(this);
		// btn_uber = (Button) findViewById(R.id.button_ubernehmen);
		// btn_uber.setOnClickListener(this);
		// btn_beenden = (Button) findViewById(R.id.button_beenden);
		// btn_beenden.setOnClickListener(this);
		btn_filepicker_ringtone  = (Button) findViewById(R.id.button_filepicker_ringtone);
		btn_filepicker_ringtone.setOnClickListener(this);
		textview1 = (TextView) findViewById(R.id.field_value_dpi);
		textview2 = (TextView) findViewById(R.id.field_value_clientid);
		//textview3 = (TextView) findViewById(R.id.field_value_rw);
		textView_ntp = (TextView) findViewById(R.id.textView_ntp);
		edittext_dpi = (EditText) findViewById(R.id.edittext_dpi);
		edittext_clientid = (EditText) findViewById(R.id.edittext_clientid);
		edittext_ntp = (EditText) findViewById(R.id.edittext_ntp);
		edittext_ringtone = (EditText) findViewById(R.id.edittext_ringtone);
		edittext_notification = (EditText) findViewById(R.id.edittext_notification);
		edittext_alarm = (EditText) findViewById(R.id.edittext_alarm);
		textview_ringtone = (TextView) findViewById(R.id.field_value_ringtone);
		textview_notification = (TextView) findViewById(R.id.field_value_notification);
		textview_sms = (TextView) findViewById(R.id.textview_sms);
		textview_alarm = (TextView) findViewById(R.id.field_value_alarm);
		checkbox_dpi = (CheckBox) findViewById(R.id.checkbox_dpi);
		checkbox_clientid = (CheckBox) findViewById(R.id.checkbox_clientid);
		checkbox_ntp = (CheckBox) findViewById(R.id.checkbox_ntp);
		checkbox_ringtone = (CheckBox) findViewById(R.id.checkbox_ringtone);
		checkbox_notification = (CheckBox) findViewById(R.id.checkbox_notification);
		checkbox_alarm = (CheckBox) findViewById(R.id.checkbox_alarm);
		progressbar1 = (ProgressBar) findViewById(R.id.progressBar1);
		
		ActionAktualisieren(this);
	}

	// Men� erstellen
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
	    return true;
	}
	
	// Menu 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    	case R.id.menu_aktualisieren:
	    		ActionAktualisieren(this);
	    		return true;
/**	    	case R.id.menu_mount_rw:
	        	utils.remount("rw");
	            return true;
	    	case R.id.menu_mount_ro:
	        	utils.remount("ro");
	            return true; */
	        case R.id.menu_ubernehmen:
	        	ActionUbernehmen(this);
	            return true;
	        case R.id.menu_reboot:
	        	utils.reboot(this);
	            return true;
	        case R.id.menu_beenden:
	        	ActionBeenden(this);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	@Override
	public void onClick(View view) {
		switch(view.getId()){
//			case R.id.button_aktualisieren:
//				ActionAktualisieren(this);
//				break;				
//			case R.id.button_ubernehmen:
//				ActionUbernehmen(this);				
//				break;
//			case R.id.button_beenden:
//				ActionBeenden(this);
//				break;
			case R.id.button_filepicker_ringtone:
				//
				break;
		}
	}
	
	public void ActionAktualisieren(MainActivity mainActivity) {
		progressbar1.setVisibility(View.VISIBLE);		
		String Path_build_prop = "/system/build.prop";
		// String statuswert[] = new String[1];
		String readwerte[] = new String[2];
		String smstone[] = new String[1];
		String statusntp[] = new String[1];
		System.out.println("pd_ begin_readfile");
		utils.readfile(Path_build_prop, readwerte);
		textview1.setText(readwerte[0]);
		textview2.setText(readwerte[1]);
		System.out.println("pd_ begin_getNTP");
		utils.getNTP("/etc/gps.conf", statusntp);
		textView_ntp.setText(statusntp[0]);
		utils.getSMSToneDirty(smstone);
		textview_sms.setText(smstone[0]);
/**		System.out.println("pd_ begin_getSMSTone");
		try {
			//SharedPreferences prefs =PreferenceManager.getDefaultSharedPreferences(getBaseContext());
			//SharedPreferences prefs =PreferenceManager.getDefaultSharedPreferences(getApplication(com.android.mms));
			String PREF_FILE_NAME = "/data/data/com.android.mms/shared_prefs/com.android.mms_preferences.xml";
			SharedPreferences prefs = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE);
			System.out.println("pd_ set");
			System.out.println("pd_ " + prefs.getString("pref_key_ringtone", "error"));
		}
		catch (Exception e) {
			System.out.println("pd_ not set!");
		}
		File file = new File("/data/data/com.android.mms/shared_prefs", "com.android.mms_preferences.xml" );
		if (file.exists()) {
			System.out.println("pd_ EXISTIERT");
		}
		else {
			System.out.println("pd_ EXISTIERT NICHT");
		} 
		utils.getSMSTone("/data/data/com.android.mms/shared_prefs/com.android.mms_preferences.xml", smstone);
		textview_sms.setText(smstone[0]); */
		System.out.println("pd_ begin_readtones");
		// TODO: try, falls tones ung�ltig sind 
		textview_ringtone.setText(RingtoneManager.getRingtone(this, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)).getTitle(this));
		textview_notification.setText(RingtoneManager.getRingtone(this, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)).getTitle(this));
		textview_alarm.setText(RingtoneManager.getRingtone(this, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)).getTitle(this));
		System.out.println("pd_ begin_schreibstatus");
		// utils.schreibstatus(statuswert);
		// textview3.setText(statuswert[0]);		
		progressbar1.setVisibility(View.INVISIBLE);
		Toast.makeText(context, "Aktualisieren Fertig...", Toast.LENGTH_SHORT).show();
	}
	
	public void ActionUbernehmen(MainActivity mainactivity) {
		progressbar1.setVisibility(View.VISIBLE);
		String Path_build_prop = "/system/build.prop";
		String Path_ziel_prop = Environment.getExternalStorageDirectory().getPath()+"/build.prop";
		System.out.println("pd_ begin_rewrite");
		if (!utils.rewrite(Path_build_prop, Path_ziel_prop, checkbox_dpi.isChecked(), edittext_dpi.getText().toString(), checkbox_clientid.isChecked(), edittext_clientid.getText().toString())) {			
			Toast.makeText(context, "Error: rewrite. Abbruch!", Toast.LENGTH_LONG).show();
			return;
		}
		System.out.println("pd_ begin_remount");
		if (!utils.remount("rw")) {
			Toast.makeText(context, "Error: remount. Abbruch!", Toast.LENGTH_LONG).show();
			return;
		}
		System.out.println("pd_ begin_copyfile");
		if (!utils.copyfile(Path_ziel_prop, Path_build_prop)) {
			Toast.makeText(context, "Error: copyfile. Abbruch!", Toast.LENGTH_LONG).show();
			return;
		}
		if (checkbox_ntp.isChecked()) {
			if (!utils.setNTP("/etc/gps.conf", Environment.getExternalStorageDirectory().getPath()+"/gps.conf", edittext_ntp.getText().toString())) {
				Toast.makeText(context, "Error: setNTP. Abbruch!", Toast.LENGTH_LONG).show();
				return;
			}
		}
		System.out.println("pd_ begin_deletefile");
		if (!utils.deletefile(Path_ziel_prop)) {
			Toast.makeText(context, "Error: deletefile. Abbruch!", Toast.LENGTH_LONG).show();
			return;
		}
		if (!utils.remount("ro")) {
			Toast.makeText(context, "Error: remount.", Toast.LENGTH_LONG).show();
		}
		if (checkbox_ringtone.isChecked())
			if (!utils.setTone("ringtone", edittext_ringtone.getText().toString(), this)) {
				Toast.makeText(context, "Error: ringtone.", Toast.LENGTH_LONG).show();	
			}
		if (checkbox_notification.isChecked())
			if (!utils.setTone("notification", edittext_notification.getText().toString(), this)) {
				Toast.makeText(context, "Error: notification.", Toast.LENGTH_LONG).show();	
			}
		if (checkbox_alarm.isChecked())
			if (!utils.setTone("alarm", edittext_alarm.getText().toString(), this)) {
				Toast.makeText(context, "Error: alarm.", Toast.LENGTH_LONG).show();	
			}
		System.out.println("pd_ begin_reboot");
		utils.reboot(mainactivity);
		progressbar1.setVisibility(View.INVISIBLE);
		Toast.makeText(context, "�bernehmen Fertig...", Toast.LENGTH_SHORT).show();
	}
	
	public void ActionBeenden(MainActivity mainactivity) {
		System.exit(0);
	}
}

